package de.planning.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import java.util.UUID;


@SpringBootApplication
@EnableMongoRepositories
public class PlanningBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlanningBackendApplication.class, args);
    }


}
