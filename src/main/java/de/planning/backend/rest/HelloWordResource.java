package de.planning.backend.rest;

import de.planning.backend.model.Recipe;
import de.planning.backend.repositories.RecipeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.UUID;

@Controller
@RequiredArgsConstructor
public class HelloWordResource {

    private final RecipeRepository recipeRepository;

    @GetMapping("/api/1/hello")
    @ResponseBody
    public List<Recipe> helloWorld() {
        recipeRepository.save(new Recipe(UUID.randomUUID(), "Test1"));
        return recipeRepository.findAll();
    }
}
