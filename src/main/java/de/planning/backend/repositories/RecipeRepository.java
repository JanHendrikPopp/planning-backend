package de.planning.backend.repositories;

import de.planning.backend.model.Recipe;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.UUID;

public interface RecipeRepository extends PagingAndSortingRepository<Recipe, String> {

    @Override
    List<Recipe> findAll();
}
